package pingrepo

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-distributor/internal/services/distributor"
)

type repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *repository {
	return &repository{db: db}
}

func (r *repository) GetPingTestsWithTimeOutSeconds(timeout int) chan distributor.Test {
	tests := make(chan distributor.Test)
	go func() {
		defer close(tests)
		tx, err := r.db.Beginx()
		if err != nil {
			logger.Fatal("Beginx fail " + err.Error())
			return
		}

		query := `
		DECLARE c CURSOR FOR SELECT *
		FROM ping_test
		WHERE timeout_seconds=$1
		`
		_, err = tx.Exec(query, timeout)
		if err != nil {
			logger.Fatal("Exec fail " + err.Error())
			return
		}

		for {
			var test distributor.Test
			err = tx.Get(&test, "FETCH NEXT FROM c")
			if err != nil {
				if err == sql.ErrNoRows {
					break
				}
				logger.Error("tx.get fail " + err.Error())
				return
			}
			tests <- test
		}

		_, err = tx.Exec("CLOSE c")
		if err != nil {
			logger.Fatal("fail close cursor " + err.Error())
		}
		err = tx.Commit()
		if err != nil {
			logger.Fatal("fail tx.commit " + err.Error())
		}
	}()

	return tests
}

func (r *repository) GetAllTimeOutsPingTest() []int {
	var timeouts []int
	query := `SELECT DISTINCT timeout_seconds FROM ping_test;`
	err := r.db.Select(&timeouts, query)
	if err != nil {
		logger.Fatal("err select timeouts" + err.Error())
		return nil
	}
	return timeouts
}
