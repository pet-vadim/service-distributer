package pingtestbroker

import (
	"encoding/json"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-distributor/internal/services/distributor"
)

func (p *publisher) SendPingTest(t distributor.Test) {
	bytes, err := json.Marshal(t)
	if err != nil {
		logger.Error("fail marshaling msg for email " + err.Error())
	}

	queueMsg := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/json",
		Body:         bytes,
	}
	err = p.amqpDialCh.Publish(p.conf.Exchange, p.conf.QueueName, p.conf.Mandatory, p.conf.NoWait, queueMsg)
	if err != nil {
		logger.Error("failed to publish msg" + err.Error())
	}
}
