package distributor

import (
	"time"
)

type service struct {
	repo   Repository
	broker Broker
}

func New(r Repository, b Broker) Service {
	return &service{
		repo:   r,
		broker: b,
	}
}

func (s *service) StartPingTestsDaemon() {
	timeouts := s.repo.GetAllTimeOutsPingTest()
	getTests := s.repo.GetPingTestsWithTimeOutSeconds
	endlessTests := GetEndlessTests[Test](timeouts, getTests)
	for test := range endlessTests {
		s.broker.SendPingTest(test)
	}
}

// GetEndlessTests на дженериках для переиспользования в других сервисах тестов, при написании нового сервиса вынести.
func GetEndlessTests[T any](timeouts []int, getTests func(timeout int) chan T) chan T {
	endlessTests := make(chan T)
	// запуск в goroutine, чтобы сразу вернуть управление
	go func() {
		// итерация по тестам с разными промежутками, например, тест каждые 60 сек, 600 сек и тд
		for _, timeout := range timeouts {
			// запускаем goroutine на каждый промежуток времени, для параллельного чтения из бд
			go func(t int) {
				// Бесконечный цикл, в котором постоянно забираем все тесты одного интервала через этот же интервал.
				// Например, забираем все тесты, которые должны быть выполнены каждые 60 секунд каждые 60 сек.
				for {
					// Запуск в goroutine для того чтобы сразу начать считать время.
					go func() {
						tests := getTests(t)
						for test := range tests {
							endlessTests <- test
						}
					}()
					time.Sleep(time.Second * time.Duration(t))
				}
			}(timeout)
		}
	}()

	return endlessTests
}
