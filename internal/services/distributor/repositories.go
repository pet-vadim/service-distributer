package distributor

// В случае ошибок бд инициируем падение всего приложения.
// В теории не должно быть ошибок и их лучше отследить отловить сразу
// todo прикрутить errGroup и перезапускать только отдельные группы goroutine

type Repository interface {
	GetPingTestsWithTimeOutSeconds(timeout int) chan Test
	GetAllTimeOutsPingTest() []int
}
