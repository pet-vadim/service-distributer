package distributor

type Test struct {
	ID             string `json:"id" db:"id"`
	UserID         string `json:"user_id" db:"user_id"`
	Name           string `json:"name" db:"name"`
	URL            string `json:"url" db:"url"`
	WontStatusCode int    `json:"wont_status_code" db:"wont_status_code"`
	FailMsg        string `json:"fail_msg" db:"fail_msg"`
	TimeOutSeconds int    `json:"timeout_seconds" db:"timeout_seconds"`
}
