package main

import (
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-distributor/internal/repositories"
	pingrepo "gitlab.com/pet-vadim/service-distributor/internal/repositories/ping"
	"gitlab.com/pet-vadim/service-distributor/internal/services/distributor"
	pingtestbroker "gitlab.com/pet-vadim/service-distributor/internal/transport/distributor"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func main() {
	logger.Info("start service-distributor app")

	db := repositories.New(getDBConfig())
	repo := pingrepo.New(db)
	broker := pingtestbroker.NewPublisher(GetBrokerConf())
	service := distributor.New(repo, broker)

	go func() {
		service.StartPingTestsDaemon()
	}()

	// Listen for the interrupt signal.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	logger.Info("received os.signal: " + (<-quit).String())

	if err := db.Close(); err != nil {
		logger.Fatal("fail db connection close " + err.Error())
	}

	broker.Close()

	logger.Info("exit app")
}

func getDBConfig() *repositories.Config {
	return &repositories.Config{
		DBUsername: os.Getenv("DB_USERNAME"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBHost:     os.Getenv("DB_HOST"),
		DBPort:     os.Getenv("DB_PORT"),
		DBTable:    os.Getenv("DB_TABLE"),
	}
}

func GetBrokerConf() *pingtestbroker.Config {
	return &pingtestbroker.Config{
		DataSource: os.Getenv("BROKER_DATA_SOURCE"),
		QueueName:  os.Getenv("BROKER_QUEUE_NAME"),
		Mandatory:  toBool(os.Getenv("BROKER_MANDATORY")),
		Durable:    toBool(os.Getenv("BROKER_DURABLE")),
		AutoDelete: toBool(os.Getenv("BROKER_AUTO_DELETE")),
		AutoAck:    toBool(os.Getenv("BROKER_AUTO_ACK")),
		Exclusive:  toBool(os.Getenv("BROKER_EXCLUSIVE")),
		NoLocal:    toBool(os.Getenv("BROKER_NO_LOCAL")),
		NoWait:     toBool(os.Getenv("BROKER_NO_WAIT")),
		Args:       nil,
	}
}

func toBool(v string) bool {
	resBool, err := strconv.ParseBool(v)
	if err != nil {
		logger.Fatal("fail convert env to bool" + err.Error())
	}
	return resBool
}
